<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleMembersCreateMembersFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        "user" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => "\Anomaly\UsersModule\User\UserModel",
            ],
        ],
        "year" => "anomaly.field_type.integer",
        "active_at" => "anomaly.field_type.datetime",
    ];

}
