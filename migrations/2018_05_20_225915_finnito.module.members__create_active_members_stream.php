<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleMembersCreateActiveMembersStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'active_members',
         'title_column' => 'user_id',
         'translatable' => false,
         'trashable' => false,
         'searchable' => false,
         'sortable' => false,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        "user",
        "year",
        "active_at",
    ];
}
