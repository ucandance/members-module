<?php

return [
    'title'       => 'Members',
    'name'        => 'Members Module',
    'description' => 'Extra layer on top of the Users Module to manage which users are active for a given year at UCanDance.'
];
