<?php

return [
    'active_members' => [
        'name'   => 'Active members',
        'option' => [
            'read'   => 'Can read active members?',
            'write'  => 'Can create/edit active members?',
            'delete' => 'Can delete active members?',
        ],
    ],
];
