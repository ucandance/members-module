<?php

return [
    'active_members' => [
        'read',
        'write',
        'delete',
    ],
];
