<?php namespace Finnito\MembersModule\User;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Anomaly\Streams\Platform\Entry\EntryObserver;
use Finnito\MembersModule\User\Event\UserWasRestored;
use Anomaly\Streams\Platform\Message\MessageBag;

// use Anomaly\UsersModule\User\Event\UserWasCreated;
// use Anomaly\UsersModule\User\Event\UserWasDeleted;
// use Anomaly\UsersModule\User\Event\UserWasUpdated;

/**
 * Class UserObserver
 *
 * @link          http://pyrocms.com/
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Ryan Thompson <ryan@pyrocms.com>
 */
class MemberObserver extends EntryObserver {
    public function restored(EntryInterface $entry) {
        $this->events->fire(new UserWasRestored($entry));
        parent::restored($entry);
    }
}
