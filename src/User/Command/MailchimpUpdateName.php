<?php namespace Finnito\MembersModule\User\Command;

use Anomaly\UsersModule\User\UserModel;
use Anomaly\Streams\Platform\Message\MessageBag;
use DrewM\MailChimp\MailChimp;

class MailchimpUpdateName
{

    protected $user;
    protected $values;

    public function __construct(UserModel $user, $values)
    {
        $this->user = $user;
        $this->newsletter = new MailChimp(env("MAILCHIMP_APIKEY"));
        $this->list = env("MAILCHIMP_LIST_ID");
        $this->values = $values;
    }

    public function handle(MessageBag $bag)
    {
        if (!empty($this->newsletter)) {
            $hash = md5(strtolower($this->user->email));
            $options = [
                'email_address' => $this->user->email,
                'status' => 'subscribed',
                'email_type' => 'html',
                "merge_fields" => [
                    "FNAME" => $this->values["first_name"],
                    "LNAME" => $this->values["last_name"],
                ],
            ];
            $this->newsletter->put("lists/{$this->list}/members/{$hash}", $options);
            // $newsletter->subscribeOrUpdate(
            //     $this->user->email,
            //     [
            //         "FNAME" => $this->user->first_name,
            //         "LNAME" => $this->user->last_name,
            //         "subscribers"
            //     ]
            // );
            if ($this->newsletter->success()) {
                $bag->success("Name updated on Mailchimp!");
            } else {
                $bag->error($this->newsletter->getLastError());
            }
        }
    }
}
