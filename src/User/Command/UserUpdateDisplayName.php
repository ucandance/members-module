<?php namespace Finnito\MembersModule\User\Command;

use Anomaly\UsersModule\User\UserModel;
use Anomaly\UsersModule\User\Contract\UserRepositoryInterface;
use Finnito\MembersModule\User\Event\UpdatedProfile;
use Anomaly\Streams\Platform\Message\MessageBag;

class UserUpdateDisplayName
{

    protected $user;
    protected $values;

    public function __construct(UserModel $user, $values)
    {
        $this->user = $user;
        $this->values = $values;
    }

    public function handle(UserRepositoryInterface $users)
    {
        $displayName = $this->values["first_name"] . " " . $this->values["last_name"];
        $this->user->display_name = $displayName;
        // dd($this->user);
        $users->save($this->user);
        // $this->user->save();
    }
}
