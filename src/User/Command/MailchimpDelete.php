<?php namespace Finnito\MembersModule\User\Command;

use Anomaly\UsersModule\User\UserModel;
use Anomaly\Streams\Platform\Message\MessageBag;
// use Spatie\Newsletter\Newsletter;
use DrewM\MailChimp\MailChimp;

class MailchimpDelete
{

    protected $user;

    public function __construct(UserModel $user)
    {
        $this->user = $user;
        $this->newsletter = new MailChimp(env("MAILCHIMP_APIKEY"));
        $this->list = env("MAILCHIMP_LIST_ID");
    }

    public function handle(MessageBag $bag)
    {
        if (!empty($this->newsletter)) {
            $hash = md5(strtolower($this->user->email));
            $response = $this->newsletter->delete("lists/{$this->list}/members/{$hash}");

            // $newsletter->delete($this->user->email);

            // Bag
            if ($this->newsletter->success()) {
                $bag->success("{$this->user->email} removed from Mailchimp!");
            } else {
                $bag->error($this->newsletter->getLastError());
            }
        }
    }
}
