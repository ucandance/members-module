<?php namespace Finnito\MembersModule\User\Command;

use Anomaly\UsersModule\User\UserModel;
use Anomaly\Streams\Platform\Message\MessageBag;
// use Spatie\Newsletter\Newsletter;
use DrewM\MailChimp\MailChimp;

class MailchimpRestore
{

    protected $user;

    public function __construct(UserModel $user)
    {
        $this->user = $user;
        $this->newsletter = new MailChimp(env("MAILCHIMP_APIKEY"));
        $this->list = env("MAILCHIMP_LIST_ID");
    }

    public function handle(
        // Newsletter $newsletter,
        MessageBag $bag
    ) {
        if (!empty($this->newsletter)) {
            $bag->success("RestoreMailchimp fired");
            $hash = md5(strtolower($this->user->email));
            $options = [
                    'email_address' => $this->user->email,
                    'status' => 'subscribed',
                    'email_type' => 'html',
                    "merge_fields" => [
                        "FNAME" => $this->user->first_name,
                        "LNAME" => $this->user->last_name,
                    ],
                ];
            $result = $this->newsletter->put("lists/{$this->list}/members/{$hash}", $options);
            // $newsletter->subscribeOrUpdate($this->user->email, ["FNAME" => $this->user->first_name, "LNAME" => $this->user->last_name, "subscribers"]);

            // Bag
            if ($this->newsletter->success()) {
                $bag->success("{$this->user->email} subscribed to Mailchimp!");
            } else {
                $bag->error($this->newsletter->getLastError());
            }
        }
    }
}
