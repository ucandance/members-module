<?php namespace Finnito\MembersModule\User\Command;

use Finnito\MembersModule\User\Event\UpdatedProfile;
use Anomaly\UsersModule\User\UserModel;

class UserSetPrevData
{

    protected $user;
    protected $newsletter;
    protected $bag;

    public function __construct(UserModel $user)
    {
        $this->user = $user;
    }

    public function handle()
    {
        if (!is_null($this->user->prevdata)) {
            $this->user->setAttribute(
                "prevdata",
                json_encode(
                    array(
                        "first_name" => $this->user->first_name,
                        "last_name" => $this->user->last_name,
                        "email" => $this->user->email,
                    )
                )
            );
            $this->user->save();
        }
    }
}
