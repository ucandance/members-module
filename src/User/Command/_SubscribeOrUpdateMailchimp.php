<?php namespace Finnito\MembersModule\User\Command;

use Anomaly\UsersModule\User\Contract\UserRepositoryInterface;
use Finnito\MembersModule\User\Event\UpdatedProfile;
use Anomaly\Streams\Platform\Message\MessageBag;
use Spatie\Newsletter\Newsletter;

class SubscribeOrUpdateMailchimp {

    protected $user;
    protected $newsletter;
    protected $bag;

    public function __construct(
        Newsletter $newsletter,
        MessageBag $bag,
        UserRepositoryInterface $users
    ) {
        $this->newsletter = $newsletter;
        $this->bag = $bag;
        $this->users = $users;
    }

    public function handle(UpdatedProfile $event) {
        $user = $event->getUser();
        $prevData = json_decode($user->prevdata, true);

        // Email changed
        if ($user->email !== $prevData["email"] && (!is_null($prevData["email"]))) {
            if (!$user->newsletter) {

                // First resubscribe the old email
                $this->newsletter->subscribeOrUpdate($email = $prevData["email"], $options = array("status" => "subscribed"));
                if ($this->newsletter->lastActionSucceeded()) {
                    $this->bag->success("Old email resubscribed");
                } else {
                    $this->bag->error("Failed to resubscribe old email");
                }

                // Second update the old email to the new one
                $this->newsletter->updateEmailAddress($prevData["email"], $user->email, "finnito_development");
                if ($this->newsletter->lastActionSucceeded()) {
                    $this->bag->success("Email changed from {$prevData["email"]} to {$user->email}");
                } else {
                    $this->bag->error("Failed to change to new email");
                }

                // Third, unsubscribe the new email to maintain state
                $this->newsletter->unsubscribe($user->email);
                if ($this->newsletter->lastActionSucceeded()) {
                    $this->bag->success("New email unsubscribed");
                } else {
                    $this->bag->error("Failed to unsubscribe new email");
                }
            }
            else {
                $this->newsletter->updateEmailAddress($prevData["email"], $user->email, "finnito_development");
                if ($this->newsletter->lastActionSucceeded()) {
                    $this->bag->success("Email changed from {$prevData["email"]} to {$user->email}");
                } else {
                    $this->bag->error("Failed to change to new email");
                }
            }
        }

        // First or last name changed
        else if (
            ($user->first_name !== $prevData["first_name"])
            ||
            ($user->last_name !== $prevData["last_name"])
        ) {
            $this->newsletter->subscribeOrUpdate(
                $user->email,
                [
                    "FNAME" => $user->first_name,
                    "LNAME" => $user->last_name,
                    "finnito_development"
                ]
            );
            if ($this->newsletter->lastActionSucceeded()) {
                $this->bag->success("First & last name updated on Mailchimp!");
            } else {
                $this->bag->error($this->newsletter->getLastError());
            }
        }

        // Update prevdata for future checks
        $user->prevdata = json_encode(array(
            "first_name" => $user->first_name,
            "last_name" => $user->last_name,
            "email" => $user->email,
        ));
        $this->users->save($user);
    }
}
