<?php namespace Finnito\MembersModule\User\Command;

use Finnito\MembersModule\User\Event\UpdatedProfile;
use Anomaly\Streams\Platform\Message\MessageBag;
use Illuminate\Foundation\Bus\DispatchesJobs;
use DrewM\MailChimp\MailChimp;

// Load Commands
use Finnito\MembersModule\User\Command\MailchimpUpdateEmail;
use Finnito\MembersModule\User\Command\UserUpdateDisplayName;
use Finnito\MembersModule\User\Command\UserSetPrevData;

class UpdateExtraFields
{

    use DispatchesJobs;

    protected $user;
    protected $newsletter;
    protected $bag;
    protected $mailchimp;

    public function __construct(
        MessageBag $bag
    ) {
        $this->bag = $bag;
        $this->newsletter = new \DrewM\MailChimp\MailChimp(env("MAILCHIMP_APIKEY"));
    }

    public function handle(UpdatedProfile $event)
    {
        // Setup the new user data
        // & get previous data
        $user = $event->getUser();
        $prevData = json_decode($user->prevdata, true);

        $this->checkDisplayName($user, $prevData);
        $this->checkEmail($user, $prevData);

        // Update the prevData field for next time
        $this->dispatch(new UserSetPrevData($user));
    }

    private function checkDisplayName($user, $prevData)
    {
        if (($user->first_name !== $prevData["first_name"])
            ||
            ($user->last_name !== $prevData["last_name"])
        ) {
            // Need to:
            // 1. Add it to the bag
            // 2. Update the display name
            // 3. Update the name on Mailchimp

            $this->bag->success("First or last name changed!");
            $this->dispatch(new UserUpdateDisplayName($user));
            $this->dispatch(new MailchimpUpdateName($user));
        }
    }

    private function checkEmail($user, $prevData)
    {
        if ($user->email !== $prevData["email"]) {
            // Need to:
            // 1. Add it to the bag
            // 2. Change the email on Mailchimp
            $this->bag->success("Email changed!");
            $this->dispatch(new MailchimpUpdateEmail($user, $prevData, $this->newsletter));
        }
    }
}
