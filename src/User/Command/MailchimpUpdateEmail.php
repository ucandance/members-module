<?php namespace Finnito\MembersModule\User\Command;

use Anomaly\Streams\Platform\Message\MessageBag;
// use Spatie\Newsletter\Newsletter;
use Anomaly\UsersModule\User\UserModel;
use DrewM\MailChimp\MailChimp;

class MailchimpUpdateEmail
{

    protected $user;
    protected $values;

    public function __construct(
        UserModel $user,
        $values
    ) {
        $this->user = $user;
        $this->values = $values;
        // $this->prevData = $prevData;
        $this->newsletter = new MailChimp(env("MAILCHIMP_APIKEY"));
        $this->list = env("MAILCHIMP_LIST_ID");
    }

    public function handle(MessageBag $bag)
    {
        if (!empty($this->newsletter)) {
            $oldHash = md5(strtolower($this->values["email"]));
            $newHash = md5(strtolower($this->user->email));
            // dd($this->newsletter);
            /*
             * The user is not subscribed to the newsletter.
             * The API cannot change the email of someone who is unsubscribed,
             * so we need to re-subscribe them, change the email and then
             * un-subscribe them again.
             */
            if (!$this->user->newsletter) {
                // First resubscribe the old email
                // $newsletter->subscribeOrUpdate(
                //     $this->prevData["email"],
                //     array("status" => "subscribed")
                // );
                $options = [
                    'email_address' => $this->values["email"],
                    'status' => 'subscribed',
                    'email_type' => 'html',
                    "merge_fields" => [
                        "FNAME" => $this->values["first_name"],
                        "LNAME" => $this->values["last_name"],
                    ],
                ];
                $result = $this->newsletter->put("lists/{$this->list}/members/{$oldHash}", $options);

                if ($this->newsletter->success()) {
                    $bag->success("Old email resubscribed");
                } else {
                    $bag->error("Failed to resubscribe old email");
                }

                // Second update the old email to the new one
                $response = $this->newsletter->patch(
                    "lists/{$this->list}/members/{$oldHash}",
                    [
                        'email_address' => $this->values["email"],
                    ]
                );
                // $newsletter->updateEmailAddress(
                //     $this->prevData["email"],
                //     $this->user->email,
                //     "subscribers"
                // );
                if ($this->newsletter->success()) {
                    $bag->success("Email changed from {$this->values["email"]} to {$this->user->email}");
                } else {
                    $bag->error("Failed to change to new email");
                }

                // Third, unsubscribe the new email to maintain state
                $response = $this->newsletter->patch(
                    "lists/{$this->list}/members/{$newHash}",
                    [
                        'status' => 'unsubscribed',
                    ]
                );
                // $newsletter->unsubscribe($this->user->email);
                if ($this->newsletter->success()) {
                    $bag->success("New email unsubscribed");
                } else {
                    $bag->error("Failed to unsubscribe new email");
                }

                // SUH SLOW.
            }
            /*
             * The user is subscribed to the newsletter,
             * so it is more simple. We just need to update
             * their email address.
             */
            else {
                $response = $this->newsletter->patch(
                    "lists/{$this->list}/members/{$oldHash}",
                    [
                        'email_address' => $this->values["email"],
                    ]
                );
                // dd($response);
                // $newsletter->updateEmailAddress(
                //     $this->prevData["email"],
                //     $this->user->email,
                //     "subscribers"
                // );
                if ($this->newsletter->success()) {
                    $bag->success("Email changed from {$this->user->email} to {$this->values['email']}");
                } else {
                    $bag->error("Failed to change to new email");
                }
            }
        }
    }
}
