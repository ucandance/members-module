<?php namespace Finnito\MembersModule\User\Event;

use Anomaly\UsersModule\User\Contract\UserInterface;

class UserWasRestored
{

    protected $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }
}
