<?php namespace Finnito\MembersModule\User\Listener;

use Anomaly\UsersModule\User\Contract\UserRepositoryInterface;
use Finnito\MembersModule\ActiveMember\ActiveMemberRepository;
use Anomaly\UsersModule\User\Event\UserWasCreated;

class AddUserToCurrentYear
{

    /**
     * The user repository.
     *
     * @var UserRepositoryInterface
     */
    protected $users;

    /**
     * Create a new TouchLastLogin instance.
     *
     * @param UserRepositoryInterface $users
     */
    public function __construct(ActiveMemberRepository $members)
    {
        $this->activeMemberRepository = $members;
    }

    /**
     * Handle the event.
     *
     * @param UserWasLoggedIn $event
     */
    public function handle(UserWasCreated $event)
    {
        $user = $event->getUser();
        $newMember = $this->activeMemberRepository->newInstance();
        $newMember->fill([
            "user_id" => $user->id,
            "year" => date("Y"),
            "active_at" => date("Y-m-d H:i:s"),
        ]);
        $this->activeMemberRepository->save($newMember);
        // $user->setAttribute("display_name", $user->first_name . " " . $user->last_name);
        // $user->setAttribute("username", $user->first_name . $user->last_name .);
        // $this->users->save($user);
    }
}
