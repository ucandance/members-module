<?php namespace Finnito\MembersModule\User\Listener;

use Finnito\MembersModule\User\Event\UserWasRestored;
use Finnito\MembersModule\User\Command\MailchimpRestore;
// use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Anomaly\Streams\Platform\Message\MessageBag;

class UserRestored
{
    use DispatchesJobs;

    // protected $dispatch;
    protected $bag;

    public function __construct(MessageBag $bag)
    {
        // $this->dispatch = $dispatch;
        $this->bag = $bag;
    }

    public function handle(UserWasRestored $event)
    {
        $this->bag->success("UserRestored listener fired");
        $user = $event->getUser();
        $this->dispatch(new MailchimpRestore($user));
    }
}
