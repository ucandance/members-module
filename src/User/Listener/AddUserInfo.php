<?php namespace Finnito\MembersModule\User\Listener;

use Anomaly\UsersModule\User\Contract\UserRepositoryInterface;
use Anomaly\UsersModule\User\Event\UserWasCreated;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Finnito\MembersModule\User\Command\UserSetPrevData;

class AddUserInfo
{
    use DispatchesJobs;

    /**
     * The user repository.
     *
     * @var UserRepositoryInterface
     */
    protected $users;
    protected $dispatch;

    /**
     * Create a new TouchLastLogin instance.
     *
     * @param UserRepositoryInterface $users
     */
    public function __construct(UserRepositoryInterface $users, Dispatcher $dispatch)
    {
        $this->users = $users;
        $this->dispatch = $dispatch;
    }

    /**
     * Handle the event.
     *
     * @param UserWasLoggedIn $event
     */
    public function handle(UserWasCreated $event)
    {
        $user = $event->getUser();
        $user->setAttribute("display_name", $user->first_name . " " . $user->last_name);
        $user->setAttribute("username", $user->first_name . $user->last_name . date("Y") . md5($user->email));
        $this->users->save($user);
        $this->dispatch(new UserSetPrevData($user));
    }
}
