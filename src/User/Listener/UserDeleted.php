<?php namespace Finnito\MembersModule\User\Listener;

use Anomaly\UsersModule\User\Event\UserWasDeleted;
use Finnito\MembersModule\User\Command\MailchimpDelete;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Foundation\Bus\DispatchesJobs;

class UserDeleted
{

    use DispatchesJobs;

    protected $dispatch;

    public function __construct(
        Dispatcher $dispatch
    ) {
        $this->dispatch = $dispatch;
    }

    public function handle(UserWasDeleted $event)
    {
        $user = $event->getUser();
        $this->dispatch(new MailchimpDelete($user));
    }
}
