<?php namespace Finnito\MembersModule\User\Listener;

use Anomaly\UsersModule\User\Event\UserWasCreated;
use Finnito\MembersModule\User\Command\MailchimpSubscribe;
use Finnito\MembersModule\User\Command\UserSetPrevData;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Foundation\Bus\DispatchesJobs;

class UserJoined
{
    use DispatchesJobs;

    protected $dispatch;

    // public function __construct(
    //     Dispatcher $dispatch
    // ) {
    //     $this->dispatch = $dispatch;
    // }

    public function handle(UserWasCreated $event)
    {
        $user = $event->getUser();
        $this->dispatch(new MailchimpSubscribe($user));
        $this->dispatch(new UserSetPrevData($user));
    }
}
