<?php namespace Finnito\MembersModule\User\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;
use Anomaly\UsersModule\User\UserModel;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;

class MembersFormBuilder extends FormBuilder {

    protected $model = UserModel::class;

    protected $fields = [
        'first_name' => [
        	"type" => "anomaly.field_type.text",
            "placeholder" => "First Name",
        ],
        'last_name' => [
            "type" => "anomaly.field_type.text",
            "placeholder" => "Last Name",
        ],
        'email' => [
        	"type" => "anomaly.field_type.text",
            "placeholder" => "Email",
            "required" => true,
        ],
        // "address" => [
        //     "type" => "anomaly.field_type.text",
        //     "placeholder" => "Address",
        // ],
        // "phone_number" => [
        //     "type" => "anomaly.field_type.text",
        //     "placeholder" => "Phone Number",
        // ],
        "student" => [
            "type" => "anomaly.field_type.boolean",
        ],
        "student_number" => [
            "type" => "anomaly.field_type.integer",
            "placeholder" => "Student Number (e.g. 43192563)",
        ],
        "student_code" => [
            "type" => "anomaly.field_type.text",
            "placeholder" => "Student Code (e.g. fel22)",
        ],
        // "department" => [
        //     "type" => "anomaly.field_type.select",
        // ],
        "birthday_dance" => [
            "type" => "anomaly.field_type.boolean",
        ],
        "birthday" => [
            "type" => "anomaly.field_type.datetime",
            "config" => [
                "mode" => "date",
                "date_format" => "d/m/Y",
                "picker" => false,
            ]
        ],
        // "password",
    ];

    protected $options = [
        'redirect' => 'profile',
        "form_view" => "finnito.module.members::profile.edit",
    ];

    protected $sections = [
        // "profile" => [
        //     'view' => 'finnito.module.members::profile/edit',
        // ],
    ];

    protected $actions = [
    	"update" => [
            "text" => "Update",
            "class" => "update",
    	],
    ];
}
