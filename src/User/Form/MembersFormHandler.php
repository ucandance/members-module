<?php namespace Finnito\MembersModule\User\Form;

use Finnito\MembersModule\User\Command\UserUpdateDisplayName;
use Finnito\MembersModule\User\Command\MailchimpUpdateEmail;
use Finnito\MembersModule\User\Command\MailchimpUpdateName;
use Finnito\MembersModule\User\Command\UserSetPrevData;
use Finnito\MembersModule\User\Form\MembersFormBuilder;
use Finnito\MembersModule\User\Event\UpdatedProfile;
use Illuminate\Contracts\Auth\Guard;

use Anomaly\Streams\Platform\Message\MessageBag;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Anomaly\UsersModule\User\Contract\UserRepositoryInterface;

class MembersFormHandler
{
    use DispatchesJobs;

    protected $bag;
    protected $users;

    public function __construct(MessageBag $bag, UserRepositoryInterface $users)
    {
        $this->bag = $bag;
        $this->users = $users;
    }

    public function handle(Guard $auth, MembersFormBuilder $builder)
    {
        if (!$user = $auth->user()) {
            abort(404);
        }
        $values = $builder->getFormValues();
        $this->checkDisplayName($user, $values);
        $this->checkEmail($user, $values);

        // dd("Up to updating the user");
        // Once the check has completed, then update the profile
        foreach ($values as $key => $value) {
            $user->$key = $value;
        }
        $this->users->save($user);
    }

    private function checkDisplayName($user, $values)
    {
        if (($user->first_name !== $values["first_name"])
            ||
            ($user->last_name !== $values["last_name"])
        ) {
            // Need to:
            // 1. Add it to the bag
            // 2. Update the display name
            // 3. Update the name on Mailchimp

            $this->bag->success("First or last name changed!");
            $this->dispatch(new UserUpdateDisplayName($user, $values)); // Done
            $this->dispatch(new MailchimpUpdateName($user, $values)); // Done
        }
    }

    private function checkEmail($user, $values)
    {
        if ($user->email !== $values["email"]) {
            // Need to:
            // 1. Add it to the bag
            // 2. Change the email on Mailchimp
            $this->bag->success("Email changed!");
            $this->dispatch(new MailchimpUpdateEmail($user, $values)); // Done
        }
    }
}
