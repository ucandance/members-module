<?php namespace Finnito\MembersModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Finnito\MembersModule\User\Form\MembersFormBuilder;
use Illuminate\Contracts\Auth\Guard;
use Anomaly\UsersModule\User\Contract\UserRepositoryInterface;
use Spatie\Newsletter\Newsletter;
use Anomaly\Streams\Platform\Message\MessageBag;
use Finnito\MembersModule\ActiveMember\ActiveMemberRepository;
use Illuminate\Translation\Translator;

class ProfileController extends AdminController {
    // protected $newsletter;

    // public function __contruct(Newsletter $newsletter) {
    //     $this->newsletter = $newsletter;
    // }
    public function view(Translator $translator, Guard $auth, MembersFormBuilder $form) {
        if(!$user = $auth->user()) {
            abort(404);
        }
        $this->template->set(
            'meta_title',
            $translator->trans('finnito.module.members::breadcrumb.profile')
        );
        $this->breadcrumbs->add("Profile", "/profile");
        return $this->view->make(
            "finnito.module.members::profile.view",
            compact("user")
        );
    }

    public function edit(Translator $translator, Guard $auth, MembersFormBuilder $form) {
        if(!$user = $auth->user()) {
            abort(404);
        }
        $this->template->set(
            'meta_title',
            $translator->trans('finnito.module.members::breadcrumb.edit_profile')
        );
        $this->breadcrumbs->add("Profile", "/profile");
        // $this->breadcrumbs->add("Edit", "/profile/edit");
        // var_dump($user);
        return $form->render($user->id);
    }

    public function join(Guard $auth, ActiveMemberRepository $members, MessageBag $bag) {
        if(!$user = $auth->user()) {
            $bag->error("Not logged in.");
            abort(404);
        }
        $year = date("Y");

        if (!$currentlyActive = $members->findBy("user_id", $user->id)) {
            $newMember = $members->newInstance();
            $newMember->fill([
                "user_id" => $user->id,
                "year" => date("Y"),
                "active_at" => date("Y-m-d H:i:s"),
            ]);
            $members->save($newMember);
            $bag->success("You have joined UCanDance for {$year}!");
        } else {
            $bag->error("You were already signed up for {$year}. I'm not sure how you did that!");
        }
        return redirect('profile');
    }

    public function subscription(
        Guard $auth,
        UserRepositoryInterface $users,
        Newsletter $newsletter,
        MessageBag $bag) {
        if(!$user = $auth->user()) {
            abort(404);
        }

        // If subscribed then unsubscribe them!
        if ($user->newsletter) {
            $user->newsletter = 0;
            $newsletter->unsubscribe(
                $user->email,
                "subscribers"
            );

            // Bag
            if ($newsletter->lastActionSucceeded()) {
                $bag->success("{$user->email} unsubscribed from Mailchimp!");
            } else {
                $bag->error($newsletter->getLastError());
            }
        }

        // If unsubscribed, subscribe them!
        else {
            $user->newsletter = 1;
            $newsletter->subscribeOrUpdate(
                $user->email,
                [
                    'FNAME' => $user->first_name,
                    'LNAME' => $user->last_name
                ],
                "subscribers"
            );
            // Bag
            if ($newsletter->lastActionSucceeded()) {
                $bag->success("{$user->email} subscribed to Mailchimp!");
            } else {
                $bag->error($newsletter->getLastError());
            }
        }
        $users->save($user);
        // var_dump($user->newsletter);
        // var_dump($newsletter->getLastError());
        // var_dump($newsletter->lastActionSucceeded());
        return redirect('profile');
    }
}
