<?php namespace Finnito\MembersModule\Http\Controller\Admin;

use Finnito\MembersModule\ActiveMember\Form\ActiveMemberFormBuilder;
use Finnito\MembersModule\ActiveMember\ActiveMemberRepository;
// use Finnito\MembersModule\ActiveMember\Table\ActiveMemberTableBuilder;
// use Finnito\MembersModule\ActiveMember\Table\ActiveMultipleTableBuilder;
// use Anomaly\UsersModule\User\Table\UserTableBuilder;
// use Anomaly\UsersModule\User\Table\UserTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Finnito\MembersModule\ActiveMember\ActiveMemberModel;
use Illuminate\Routing\ResponseFactory;

class ActiveMembersController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param ActiveMemberTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index() {
        return $this->view->make(
            "finnito.module.members::admin.active"
        );
    }

    /**
     * Create a new entry.
     *
     * @param ActiveMemberFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ActiveMemberFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param ActiveMemberFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ActiveMemberFormBuilder $form, $id)
    {
        return $form->render($id);
    }

    /**
     * Base export page.
     *
     * @param
     * @return Twig admin.export view
     */
    public function export() {
        return $this->view->make(
            "finnito.module.members::admin.export.index"
        );
    }

    /**
     * Export members for the current year.
     *
     * @param ActiveMemberModel $members
     * @return Twig admin.export view
     */
    public function preview(ActiveMemberModel $members) {
        $activeMembers = $this->getActiveMembers($members);

        return $this->view->make(
            "finnito.module.members::admin.export.preview",
            [
                "members" => $activeMembers
            ]
        );
    }

    public function download(
        ActiveMemberModel $members,
        ResponseFactory $response
    ) {
        $activeMembers = $this->getActiveMembers($members);

        $year = date("Y");

        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=UCanDance_Members_{$year}.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $columns = array('First Name', 'Last Name', 'Email', 'Student Number', 'Student Code');

        $callback = function() use ($activeMembers, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($activeMembers as $member) {
                fputcsv(
                    $file,
                    array(
                        $member->first_name,
                        $member->last_name,
                        $member->email,
                        $member->student_number,
                        $member->student_code,
                    )
                );
            }
            fclose($file);
        };
        return $response->stream($callback, 200, $headers);
    }

    private function getActiveMembers(
        ActiveMemberModel $members,
        $limit = 999999
    ) {
        return $members
            ->join('users_users', 'members_active_members.user_id', '=', 'users_users.id')
            ->where("year", date("Y"))
            ->limit($limit)
            ->get(array(
                "first_name",
                "last_name",
                "email",
                "student_number",
                "student_code",
            ));
    }

    public function deleteOldMembers() {
        return $this->view->make(
            "finnito.module.members::admin.deleteOldMembers.index"
        );
    }

    public function confirmDeleteOldMembers(ActiveMemberModel $members) {
        var_dump("Do things");
        $delDate = date("Y-m-d H:i:s");
        $deletes = $members
            ->selectRaw("
                user_id,
                MAX(active_at) as lastActive,
                DATE_SUB(
                    CURDATE(),
                    INTERVAL 7 YEAR
                ) as cutoff
            ")
            ->groupBy("user_id")
            ->havingRaw("lastActive < cutoff")
            ->get(["*"]);
        foreach ($deletes as $delete) {
            $user = $delete->user;

            // Update fields
            $user->deleted_at = $delDate;
            $user->created_at = null;
            $user->created_by_id = null;
            $user->updated_at = $delDate;
            $user->updated_by_id = null;
            $user->email = hash("sha256", $user->email); // Unique??
            $user->password = null;
            $user->display_name = null;
            $user->username = hash("sha256", $user->username); // Unique
            $user->first_name = "Deleted User"; // For frontend display
            $user->last_name = null;
            $user->activated = null;
            $user->enabled = null;
            $user->last_login_at = null;
            $user->remember_token = null;
            $user->activation_code = null;
            $user->reset_code = null;
            $user->last_activity_at = null;
            $user->ip_address = null;
            $user->address = null;
            $user->phone_number = null;
            $user->student = null;
            $user->student_number = null;
            $user->student_code = null;
            $user->department = null;
            $user->birthday = null;
            $user->birthday_dance = null;
            $user->newsletter = null;
            $user->prevdata = null;

            // Save!
            $user->save();
        }
    }
}
