<?php namespace Finnito\MembersModule\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Finnito\MembersModule\User\Form\MembersFormBuilder;
use Illuminate\Contracts\Auth\Guard;

class MembersController extends PublicController
{
    public function disable()
    {
        abort(404);
    }

    public function login()
    {
        $this->template->set("meta_title", "Login");
        $this->template->set("meta_description", "Log in to UCanDance to manage your account.");
        $this->breadcrumbs->add("Login", $this->request->path());
        return $this->view->make('finnito.module.members::login');
    }

    public function register()
    {
        $this->template->set("meta_title", "Join");
        $this->template->set("meta_description", "Join UCanDance!");
        $this->breadcrumbs->add("Join", $this->request->path());
        return $this->view->make(
            "finnito.module.members::register"
        );
    }
}
