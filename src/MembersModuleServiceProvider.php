<?php namespace Finnito\MembersModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Finnito\MembersModule\ActiveMember\Contract\ActiveMemberRepositoryInterface;
use Finnito\MembersModule\ActiveMember\ActiveMemberRepository;
use Anomaly\Streams\Platform\Model\Members\MembersActiveMembersEntryModel;
use Finnito\MembersModule\ActiveMember\ActiveMemberModel;
use Illuminate\Routing\Router;
use Anomaly\Streams\Platform\View\ViewOverrides;

use Finnito\MembersModule\User\MemberObserver;

use Anomaly\UsersModule\User\Event\UserWasCreated;
use Anomaly\UsersModule\User\Event\UserWasDeleted;
use Finnito\MembersModule\User\Event\UpdatedProfile;
use Finnito\MembersModule\User\Event\UserWasRestored;

use Finnito\MembersModule\User\Listener\AddUserInfo;
use Finnito\MembersModule\User\Listener\AddUserToCurrentYear;
use Finnito\MembersModule\User\Listener\UserJoined;
use Finnito\MembersModule\User\Listener\UserDeleted;
use Finnito\MembersModule\User\Listener\UserRestored;
// use Finnito\MembersModule\User\Listener\RemoveFromMailchimp;
// use Finnito\MembersModule\User\Listener\UpdateMailchimp;

use Finnito\MembersModule\User\Command\UpdateExtraFields;
use Finnito\MembersModule\User\Command\SubscribeOrUpdateMailchimp;
use Finnito\MembersModule\User\Command\SetNewDisplayName;

use Anomaly\UsersModule\User\Login\LoginFormBuilder;
use Anomaly\UsersModule\User\Login\LoginFormFields;
use Anomaly\UsersModule\User\Login\LoginFormHandler;

use Anomaly\UsersModule\User\UserModel;


use Spatie\NewsLetter\Newsletter;

class MembersModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Additional addon plugins.
     *
     * @type array|null
     */
    protected $plugins = [
    ];

    /**
     * The addon Artisan commands.
     *
     * @type array|null
     */
    protected $commands = [];

    /**
     * The addon's scheduled commands.
     *
     * @type array|null
     */
    protected $schedules = [];

    /**
     * The addon API routes.
     *
     * @type array|null
     */
    protected $api = [];

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        // Core admin panel routes
        'admin/members'           => 'Finnito\MembersModule\Http\Controller\Admin\ActiveMembersController@index',
        'admin/members/create'    => 'Finnito\MembersModule\Http\Controller\Admin\ActiveMembersController@create',
        'admin/members/edit/{id}' => 'Finnito\MembersModule\Http\Controller\Admin\ActiveMembersController@edit',

        // Add profile routes
        'profile'                              => [
            'as'   => 'finnito.module.members::profile',
            'uses' => 'Finnito\MembersModule\Http\Controller\Admin\ProfileController@view',
        ],
        'profile/edit'                              => [
            'as'   => 'finnito.module.members::profile.edit',
            'uses' => 'Finnito\MembersModule\Http\Controller\Admin\ProfileController@edit',
        ],
        "profile/join" => "Finnito\MembersModule\Http\Controller\Admin\ProfileController@join",

        // Override built-in routes
        "join" => [
            "as" => "finnito.module.members::register",
            "uses" => "Finnito\MembersModule\Http\Controller\MembersController@register",
        ],
        "login" => [
            "as" => "finnito.module.members::login",
            "uses" => "Finnito\MembersModule\Http\Controller\MembersController@login",
        ],
        'users/password/reset'               => [
            'as'   => 'finnito.module.members::password.reset',
            'uses' => 'Finnito\MembersModule\Http\Controller\MembersPasswordController@reset',
        ],
        'users/password/forgot'              => [
            'as'   => 'finnito.module.members::password.forgot',
            'uses' => 'Finnito\MembersModule\Http\Controller\MembersPasswordController@forgot',
        ],

        // Subscription Routes
        "profile/updateSubscription" => "Finnito\MembersModule\Http\Controller\Admin\ProfileController@subscription",

        // Disable routes
        "register" => [
            "as" => "finnito.module.members",
            "uses" => "Finnito\MembersModule\Http\Controller\MembersController@disable",
        ],
        '@{username}' => [
            "as" => "finnito.module.members",
            "uses" => "Finnito\MembersModule\Http\Controller\MembersController@disable",
        ],

        // Backend Routes
        "admin/members/export" => "Finnito\MembersModule\Http\Controller\Admin\ActiveMembersController@export",
        "admin/members/export/preview" => "Finnito\MembersModule\Http\Controller\Admin\ActiveMembersController@preview",
        "admin/members/export/download" => "Finnito\MembersModule\Http\Controller\Admin\ActiveMembersController@download",
        "admin/members/deleteOldMembers" => "Finnito\MembersModule\Http\Controller\Admin\ActiveMembersController@deleteOldMembers",
        "admin/members/deleteOldMembers/confirm" => "Finnito\MembersModule\Http\Controller\Admin\ActiveMembersController@confirmDeleteOldMembers",
    ];

    /**
     * The addon middleware.
     *
     * @type array|null
     */
    protected $middleware = [
        //Finnito\MembersModule\Http\Middleware\ExampleMiddleware::class
    ];

    /**
     * Addon group middleware.
     *
     * @var array
     */
    protected $groupMiddleware = [
        //'web' => [
        //    Finnito\MembersModule\Http\Middleware\ExampleMiddleware::class,
        //],
    ];

    /**
     * Addon route middleware.
     *
     * @type array|null
     */
    protected $routeMiddleware = [];

    /**
     * The addon event listeners.
     *
     * @type array|null
     */
    protected $listeners = [
        UserWasCreated::class => [
            AddUserInfo::class,
            AddUserToCurrentYear::class,
            UserJoined::class,
        ],
        UserWasDeleted::class => [
            UserDeleted::class,
        ],
        UserWasRestored::class => [
            UserRestored::class,
        ],
    ];

    /**
     * The addon alias bindings.
     *
     * @type array|null
     */
    protected $aliases = [
        //'Example' => Finnito\MembersModule\Example::class
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [
        MembersActiveMembersEntryModel::class => ActiveMemberModel::class,
        "profile" => "Finnito\MembersModule\User\Form\MembersFormBuilder",
        "joinCurrentYear" => "Finnito\MembersModule\ActiveMember\Form\ActiveMemberFormBuilder",
    ];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [
        ActiveMemberRepositoryInterface::class => ActiveMemberRepository::class,
    ];

    /**
     * Additional service providers.
     *
     * @type array|null
     */
    protected $providers = [
        //\ExamplePackage\Provider\ExampleProvider::class
    ];

    /**
     * The addon view overrides.
     *
     * @type array|null
     */
    protected $overrides = [
        "anomaly.module.users::users.view" => "finnito.module.members::users.view",
        "anomaly.module.users::login" => "finnito.module.members::login",
    ];

    /**
     * The addon mobile-only view overrides.
     *
     * @type array|null
     */
    protected $mobile = [
        //'streams::errors/404' => 'module::mobile/errors/404',
        //'streams::errors/500' => 'module::mobile/errors/500',
    ];

    /**
     * Register the addon.
     */
    public function register(UserModel $model)
    {
        // Run extra pre-boot registration logic here.
        // Use method injection or commands to bring in services.
        $model->bind(
            "signups",
            function () {
                return $this
                    ->hasMany(ActiveMemberModel::class, "user_id");
            }
        );

        $model->bind(
            "getSignups",
            function () {
                return $this
                    ->hasMany(ActiveMemberModel::class, "user_id")
                    ->get();
            }
        );

        $model->bind(
            "isSignedUp",
            function () {
                $year = date("Y");
                $currentSignup = $this
                    ->hasMany(ActiveMemberModel::class, "user_id")
                    ->whereYear("active_at", "=", $year)
                    ->first();
                    return (!is_null($currentSignup));
                // return (count($currentSignup) > 0);
            }
        );

        $model->bind(
            "activeYears",
            function () {
                $years = $this
                    ->hasMany(ActiveMemberModel::class, "user_id")
                    ->orderBy("active_at", "DESC")
                    ->get()
                    ->pluck("year");
                return $years->implode(", ");
                // dd($years);
                // return implode(", ", $years);
                // return $years;
            }
        );
    }

    /**
     * Boot the addon.
     */
    public function boot()
    {
        // Run extra post-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Map additional addon routes.
     *
     * @param Router $router
     */
    public function map()
    {
        // Register dynamic routes here for example.
        // Use method injection or commands to bring in services.
    }
}
