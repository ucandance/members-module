<?php namespace Finnito\MembersModule\ActiveMember;

use Anomaly\Streams\Platform\Entry\EntryCriteria;
use Illuminate\Support\Facades\DB;

class ActiveMemberCriteria extends EntryCriteria
{
	public function monthlySignups() {
		$collection = $this->query
                  ->selectRaw(
                  	"count('id') as numSignups,
                  	MONTHNAME(active_at) as month,
                  	YEAR(active_at) as year,
                        CONCAT(YEAR(active_at), MONTHNAME(active_at)) as identifier"
                  )
                  ->groupBy(DB::raw("MONTH(active_at)"))
                  ->get(["*"]);
            $keyed = $collection->keyBy("identifier");
            $keyed->all();
		return $keyed;
	}

      public function yearlySignups() {
            $collection = $this->query
                  ->selectRaw(
                        "count(id) as numSignups,
                        YEAR(active_at) as year"
                  )
                  ->groupBy(DB::raw("YEAR(active_at)"))
                  ->get(["*"]);

            $keyed = $collection->keyBy('year');
            $keyed->all();

            return $keyed;
      }

      public function currentYearSignupsByMonth() {
            $collection = $this->query
                  ->selectRaw("
                        MONTHNAME(active_at) as month,
                        count(id) as numSignups
                  ")
                  ->whereRaw("YEAR(active_at)", (int) date("Y"))
                  ->groupBy(DB::raw("MONTHNAME(active_at)"))
                  ->get(["*"]);
            $keyed = $collection->keyBy('month');
            $keyed->all();

            return $keyed;
      }

      public function membersToBeDeleted() {
            // SELECT
            //       user_id,
            //       MAX(active_at) as lastActive,
            //       DATE_SUB(
            //             CURDATE(),
            //             INTERVAL 7 YEAR
            //       ) as cutoff
            // FROM
            //       pyrocms.pyrotest_members_active_members
            // GROUP BY
            //       user_id
            // HAVING
            //       lastActive < cutoff;

            $this->query
                  ->selectRaw("
                        user_id,
                        MAX(active_at) as lastActive,
                        DATE_SUB(
                              CURDATE(),
                              INTERVAL 7 YEAR
                        ) as cutoff
                  ")
                  ->join("users_users", "members_active_members.user_id", "=", "users_users.id")
                  ->whereRaw("deleted_at IS NULL")
                  ->groupBy("user_id")
                  ->havingRaw("lastActive < cutoff");
            return $this;
      }

}
