<?php namespace Finnito\MembersModule\ActiveMember\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface ActiveMemberRepositoryInterface extends EntryRepositoryInterface
{

}
