<?php namespace Finnito\MembersModule\ActiveMember\Form;

use Illuminate\Routing\Redirector;
use Symfony\Component\HttpFoundation\Response;
use Finnito\MembersModule\ActiveMember\ActiveMemberRepository;
use Input;
use Session;
use Request;
use Illuminate\Contracts\Auth\Guard;
use Anomaly\Streams\Platform\Message\MessageBag;

class ActiveMemberFormHandler {
    protected $activeMemberRepository;

    public function __construct(
        ActiveMemberRepository $activeMemberRepository
    ) {
        $this->activeMemberRepository = $activeMemberRepository;
    }

    public function handle(
        Guard $auth,
        Redirector $redirect,
        MessageBag $bag
    ) {
        if(!$user = $auth->user()) {
            $bag->error("Not logged in.");
            abort(404);
        }
        $year = date("Y");
        $newMember = $this->activeMemberRepository->newInstance();
        $newMember->fill([
            "user_id" => $user->id,
            "year" => date("Y"),
            "active_at" => date("Y-m-d H:i:s"),
        ]);
        $this->activeMemberRepository->save($newMember);
        $bag->success("You have joined UCanDance for {$year}!");
    }
}