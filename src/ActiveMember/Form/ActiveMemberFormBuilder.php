<?php namespace Finnito\MembersModule\ActiveMember\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

class ActiveMemberFormBuilder extends FormBuilder
{

    protected $handler = "Finnito\MembersModule\ActiveMember\Form\ActiveMemberFormHandler@handle";


    /**
     * The form fields.
     *
     * @var array|string
     */
    protected $fields = [
        "correct" => [
            "type" => "anomaly.field_type.boolean",
            "label" => "My profile is correct and up to date",
        ],
    ];

    /**
     * Additional validation rules.
     *
     * @var array|string
     */
    protected $rules = [
        "correct" => [
            "required",
        ],
    ];

    /**
     * Fields to skip.
     *
     * @var array|string
     */
    protected $skips = [
        "user",
        "year",
        "active_at",
    ];

    /**
     * The form actions.
     *
     * @var array|string
     */
    protected $actions = [
        "join" => [
            "button" => "save",
            "text" => "finnito.module.members::button.join"
        ],
    ];

    /**
     * The form buttons.
     *
     * @var array|string
     */
    protected $buttons = [];

    /**
     * The form options.
     *
     * @var array
     */
    protected $options = [];

    /**
     * The form sections.
     *
     * @var array
     */
    protected $sections = [];

    /**
     * The form assets.
     *
     * @var array
     */
    protected $assets = [];

}
