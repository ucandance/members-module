<?php namespace Finnito\MembersModule\ActiveMember;

use Finnito\MembersModule\ActiveMember\Contract\ActiveMemberRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class ActiveMemberRepository extends EntryRepository implements ActiveMemberRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var ActiveMemberModel
     */
    protected $model;

    /**
     * Create a new ActiveMemberRepository instance.
     *
     * @param ActiveMemberModel $model
     */
    public function __construct(ActiveMemberModel $model)
    {
        $this->model = $model;
    }
}
