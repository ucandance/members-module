<?php namespace Finnito\MembersModule\ActiveMember;

use Finnito\MembersModule\ActiveMember\Contract\ActiveMemberInterface;
use Finnito\MembersModule\ActiveMember\Contract\ActiveMemberRepositoryInterface;
use Anomaly\Streams\Platform\Model\Members\MembersActiveMembersEntryModel;

class ActiveMemberModel extends MembersActiveMembersEntryModel implements ActiveMemberInterface
{

	public function activeYears() {
		$signups = $this
			->where("user_id", $this->user_id)
			->select("year")
			->orderBy("year", "DESC")
			->get(["year"])
			->pluck("year");

		$years = array();
		foreach ($signups as $entry) {
			array_push($years, $entry);
		}
		// var_dump($signups);
		// $years = implode(", ", $signups);
		// return $years;
		return implode(", ", $years);
	}

}
